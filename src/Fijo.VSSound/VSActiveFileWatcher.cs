using System.Runtime.InteropServices;
using EnvDTE;
using Thread = System.Threading.Thread;

namespace Fijo.VSSound {
	public class VSActiveFileWatcher {
		public void Start() {
			var currentFile = "";
			while (true) {
				Thread.Sleep(250);
				DTE dte;
				string file;
				try {
					dte = (DTE) Marshal.GetActiveObject("VisualStudio.DTE");
					if (dte == null) continue;
					var activeDocument = dte.ActiveDocument;
					if (activeDocument == null) continue;
					file = activeDocument.FullName;
				}
				catch (COMException) {
					continue;
				}

				if (file != currentFile) {
					currentFile = file;
					OnActiveFileUpdated(currentFile);
				}
			}
		}

		private void OnActiveFileUpdated(string currentFile) {
			if (ActiveFileUpdated != null) ActiveFileUpdated(currentFile);
		}

		public event FileUpdatedEvent ActiveFileUpdated;
	}
}