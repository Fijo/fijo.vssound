using System.IO;
using System.Linq;

namespace Fijo.VSSound {
	public class VSSound {
		private readonly VSActiveFileWatcher _activeFileWatcher = new VSActiveFileWatcher();
		private readonly ManagedSoundPlayer _managedSoundPlayer = new ManagedSoundPlayer();

		public VSSound() {
			_activeFileWatcher.ActiveFileUpdated += file => {
			    _managedSoundPlayer.Stop();
			    var musicFile = TryGetMusicFile(file);
			    if (musicFile != string.Empty) _managedSoundPlayer.Play(musicFile);
			};
		}

		public void Start() {
			_activeFileWatcher.Start();
		}

		private string TryGetMusicFile(string currentFile) {
			if (!File.Exists(currentFile)) return string.Empty;
			var firstLine = File.ReadAllLines(currentFile).FirstOrDefault();
			if (firstLine == null || !firstLine.StartsWith("//BS:") && firstLine.Length > 5) return string.Empty;
			var musicFile = firstLine.Substring(5).Trim();
			return musicFile;
		}
	}
}