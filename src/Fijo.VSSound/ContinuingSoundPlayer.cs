using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Audio.Player;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.VSSound {
	public class ContinuingSoundPlayer {
		private readonly IMusicPlayerFacade _musicPlayerFacade = Kernel.Resolve<IMusicPlayerFacade>();
		private readonly IDictionary<string, double> _positions = new Dictionary<string, double>();
		private readonly MusicPlayer _musicPlayer;
		private string _currentFile;
		private readonly double _minTimeLeftToContinue = Kernel.Resolve<IConfigurationService>().Get<double>(string.Format("{0}.MinTimeLeftToContinue", typeof (ContinuingSoundPlayer).FullName));

		public ContinuingSoundPlayer() {
			_musicPlayer = _musicPlayerFacade.Create();
		}

		public void Play(string file) {
			Stop();
			_currentFile = file;
			_musicPlayerFacade.SetUrl(_musicPlayer, file);
			MayContinueAtPosition(file);
			_musicPlayerFacade.Play(_musicPlayer);
		}

		public void Stop() {
			if (!_musicPlayerFacade.IsPlaying(_musicPlayer)) return;
			SaveCurrentPosition();
			_musicPlayerFacade.Stop(_musicPlayer);
		}

		private void MayContinueAtPosition(string file) {
			double position;
			if (!_positions.TryGetValue(file, out position)) return;

			var duration = _musicPlayerFacade.GetDuration(_musicPlayer);
			if (position + _minTimeLeftToContinue > duration) return;
			_musicPlayerFacade.SetPosition(_musicPlayer, position);

		}


		private void SaveCurrentPosition() {
			Debug.Assert(_currentFile != null, "_currentFile != null");
			_positions[_currentFile] = _musicPlayerFacade.GetPosition(_musicPlayer);
		}

		~ContinuingSoundPlayer() {
			_musicPlayerFacade.Close(_musicPlayer);
		}
	}
}