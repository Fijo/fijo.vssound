﻿using Fijo.VSSound.Properties;

namespace Fijo.VSSound {
	internal class Program {
		private static void Main(string[] args) {
			new InternalInitKernel().Init();
			new VSSound().Start();
		}
	}
}