using Fijo.Audio.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;

namespace Fijo.VSSound.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new LightContribInjectionModule(), new AudioInjectionModule());
		}
	}
}