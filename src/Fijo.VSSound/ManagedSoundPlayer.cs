using System.Collections.Generic;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Repositories;

namespace Fijo.VSSound {
	public class ManagedSoundPlayer {
		private readonly ContinuingSoundPlayer _continuingSoundPlayer = new ContinuingSoundPlayer();
		private readonly IDictionary<string, string> _musicStore = new Dictionary<string, string>();

		public ManagedSoundPlayer() {
			var musicFolder = Kernel.Resolve<IConfigurationService>().Get<string>(string.Format("{0}.MusicFolder", typeof (ManagedSoundPlayer).FullName));
			var allFileRepository = Kernel.Resolve<AllFileRepository>();
			var pairFactory = Kernel.Resolve<IPairFactory>();
			var files = allFileRepository.Get(musicFolder);
			_musicStore = files.Select(x => pairFactory.KeyValuePair(Path.GetFileNameWithoutExtension(x), x)).DistinctBy(x => x.Key).ToDictionary();
		}

		public void Play(string file) {
			string path;
			if (_musicStore.TryGetValue(file, out path)) _continuingSoundPlayer.Play(path);
		}

		public void Stop() {
			_continuingSoundPlayer.Stop();
		}
	}
}